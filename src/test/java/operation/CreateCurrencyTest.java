package operation;

import org.junit.Assert;
import org.junit.Test;

public class CreateCurrencyTest {
    @Test
    public void getInstanceNotEqualTest() {
        Operation operation1 = CreateCurrency.getInstance();
        Operation operation2 = CreateCurrency.getInstance();

        Assert.assertEquals(operation1, operation2);
    }

    @Test
    public void getInstanceNotNullTest() {
        Operation operation1 = CreateCurrency.getInstance();

        Assert.assertNotNull(operation1);
    }
}
