package operation;

import org.junit.Assert;
import org.junit.Test;

public class CreateMeatTest {

    @Test
    public void getInstanceNotEqualTest() {
        Operation operation1 = CreateMeat.getInstance();
        Operation operation2 = CreateMeat.getInstance();

        Assert.assertEquals(operation1, operation2);
    }

    @Test
    public void getInstanceNotNullTest() {
        Operation operation1 = CreateMeat.getInstance();

        Assert.assertNotNull(operation1);
    }
}
