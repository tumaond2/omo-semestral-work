package simulation;

import commodity.Commodity;
import operation.Operation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import product.Product;
import transactionCreator.TransactionCreator;

@SuppressWarnings("FieldCanBeLocal")
public class RequestTest {
    private Commodity commodity;
    private Commodity source;
    private TransactionCreator originator;
    private Operation operation;
    @SuppressWarnings("SpellCheckingInspection")
    private Request prereq;
    private Request request;
    private Product product;
    private Simulation context;

    @Before
    public void init() {
        commodity = Mockito.mock(Commodity.class);
        originator = Mockito.mock(TransactionCreator.class);
        operation = Mockito.mock(Operation.class);
        prereq = Mockito.mock(Request.class);
        product = Mockito.mock(Product.class);
        context = Mockito.mock(Simulation.class);
        source = Mockito.mock(Commodity.class);
        request = new Request(commodity, source, operation, originator, product, context);
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    public void phase1NoPrereqTest() {
        request.decreaseTickCount();
        Assert.assertEquals(-1, request.getTicksLeft().intValue());
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    public void phase1PrereqTest() {
        request.setPrerequisite(prereq);
        request.decreaseTickCount();

        Mockito.verify(prereq, Mockito.times(1)).isDone();
        Assert.assertEquals(0, request.getTicksLeft().intValue());
    }
}
