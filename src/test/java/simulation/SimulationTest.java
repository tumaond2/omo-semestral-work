package simulation;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import transactionCreator.TransactionCreator;

public class SimulationTest {

    @Test
    public void simulationTickTest() {
        Simulation simulation = new Simulation();
        Assert.assertEquals(0, simulation.getActualTick().intValue());

        simulation.nextTick();
        Assert.assertEquals(1, simulation.getActualTick().intValue());
    }

    @Test
    public void simulationSubscriptionTest() {
        Simulation simulation = new Simulation();
        TransactionCreator test = Mockito.mock(TransactionCreator.class);
        simulation.subscribe(test);
        simulation.nextTick();

        Mockito.verify(test, Mockito.times(1)).nextTickPhase1();
        Mockito.verify(test, Mockito.times(1)).nextTickPhase2();
    }

}
