package simulation;

import channel.*;
import commodity.Commodity;
import operation.CreateCurrency;
import operation.MakeMeal;
import operation.Operation;
import org.junit.Before;
import org.junit.Test;
import product.Product;
import report.ReportHandler;
import testHelpers.Pair;
import testHelpers.ProductPrepareHelper;
import transactionCreator.TransactionCreator;
import transactionCreator.TransactionCreatorFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("SpellCheckingInspection")
public class GeneralScenarionsTest {
    Logger logger = Logger.getLogger(GeneralScenarionsTest.class.getName());
    Simulation simulation;
    ReportHandler reportHandler;
    TransactionCreatorFactory factory;
    MeatChannel meatChannel;
    CurrencyChannel currencyChannel;
    MilkChannel milkChannel;
    CropChannel cropChannel;
    HashMap<String, Channel> channelsMap;
    ProductPrepareHelper productPrepareHelper;

    private static final Logger LOG = Logger.getLogger(GeneralScenarionsTest.class.getName());

    @Before
    public void init() {
        simulation = new Simulation();

        reportHandler = new ReportHandler(simulation);

        factory = new TransactionCreatorFactory(simulation);

        meatChannel = new MeatChannel();
        reportHandler.addChannel(meatChannel);

        currencyChannel = new CurrencyChannel();
        reportHandler.addChannel(currencyChannel);

        milkChannel = new MilkChannel();
        reportHandler.addChannel(milkChannel);

        cropChannel = new CropChannel();
        reportHandler.addChannel(cropChannel);

        channelsMap = new HashMap<>();
        channelsMap.put("Meat", meatChannel);
        channelsMap.put("Milk", milkChannel);
        channelsMap.put("Crop", cropChannel);
        channelsMap.put("Currency", currencyChannel);

        productPrepareHelper = new ProductPrepareHelper(channelsMap);
    }

    @Test
    public void case1() {
        logger.log(Level.INFO, "TEST CASE 1");

        TransactionCreator producer = factory.createBreeder();
        TransactionCreator meatProcessor = factory.createButcher();
        TransactionCreator distributor = factory.createDistributor();

        TransactionCreator cheeseMaker = factory.createMilkProcessor();

        TransactionCreator customer = factory.createCustomer();

        Product meat = customer.getMeat(meatChannel, currencyChannel);

        Product cheese = customer.getCheese(milkChannel, currencyChannel);

        Commodity money = null;
        Commodity money2 = null;
        Commodity money3 = null;
        try {
            money = CreateCurrency.getInstance().apply(null, customer);
            money.setValue(meat.getPrice());
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Customer cannot create new money");
        }
        try {
            money2 = CreateCurrency.getInstance().apply(null, customer);
            money2.setValue(meat.getPrice());
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Customer cannot create new money");
        }
        try {
            money3 = CreateCurrency.getInstance().apply(null, customer);
            money3.setValue(meat.getPrice());
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Customer cannot create new money");
        }

        customer.request(money, meat, distributor);
        customer.request(money2, cheese, distributor);
        simulation.runTicks(8);

        meat.resetCounter();
        customer.request(money3, meat, distributor);
        simulation.runTicks(8);
        logger.log(Level.INFO, "");

        // Generate reports:
        String foodChainReport = reportHandler.getFoodChainReport(new ArrayList<Product>() {{
            add(meat);
            add(cheese);
        }});
        String doubleSpendingReport = reportHandler.getDoubleSpendingReport();
        String transactionReport = reportHandler.getTransactionReport();

        // Print reports:
        logger.log(Level.INFO, foodChainReport);
        logger.log(Level.INFO, doubleSpendingReport);
        logger.log(Level.INFO, transactionReport);

        // Save reports:
        reportHandler.writeToFile(new HashMap<String, String>() {{
            put("food_chain_report_1.txt", foodChainReport);
            put("double_spending_report_1.txt", doubleSpendingReport);
            put("transaction_report_1.txt", transactionReport);
        }});

    }

    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    @Test
    public void case2() {
        TransactionCreator customer_1 = factory.createCustomer();
        TransactionCreator customer_2 = factory.createCustomer();
        TransactionCreator breeder_1 = factory.createBreeder();
        TransactionCreator breeder_2 = factory.createBreeder();
        TransactionCreator butcher_1 = factory.createButcher();
        TransactionCreator butcher_2 = factory.createButcher();
        TransactionCreator butcher_3 = factory.createButcher();
        TransactionCreator restaurant_1 = factory.createRestaurant();
        TransactionCreator distributor_1 = factory.createDistributor();
        TransactionCreator distributor_2 = factory.createDistributor();

        // Products for 1st customer:
        ArrayList<Pair<Product, TransactionCreator>> products1 = new ArrayList<>();

        products1.add(new Pair<>(customer_1.getMeat(meatChannel, currencyChannel), distributor_1));    // 0
        products1.add(new Pair<>(customer_1.getMeat(meatChannel, currencyChannel), distributor_2));    // 1
        products1.add(new Pair<>(customer_1.getMeat(meatChannel, currencyChannel), distributor_1));    // 2
        products1.add(new Pair<>(customer_1.getMeat(meatChannel, currencyChannel), distributor_2));    // 3
        products1.add(null);                                                                           // 4
        products1.add(new Pair<>(customer_1.getMeal(meatChannel, currencyChannel), restaurant_1));     // 5
        products1.add(null);                                                                           // 6
        products1.add(null);                                                                           // 7
        products1.add(null);                                                                           // 8
        products1.add(new Pair<>(customer_1.getMeal(meatChannel, currencyChannel), restaurant_1));     // 9
        products1.add(null);                                                                           //10
        products1.add(new Pair<>(customer_1.getMeat(meatChannel, currencyChannel), distributor_1));    //11
        products1.add(new Pair<>(customer_1.getMeal(meatChannel, currencyChannel), restaurant_1));     //12


        // Products for 2nd customer:
        ArrayList<Pair<Product, TransactionCreator>> products2 = new ArrayList<>();

        products2.add(null);                                                                           // 0
        products2.add(new Pair<>(customer_2.getMeat(meatChannel, currencyChannel), distributor_1));    // 1
        products2.add(null);                                                                           // 2
        products2.add(new Pair<>(customer_2.getMeal(meatChannel, currencyChannel), restaurant_1));     // 3
        products2.add(new Pair<>(customer_2.getMeal(meatChannel, currencyChannel), restaurant_1));     // 4
        products2.add(null);                                                                           // 5
        products2.add(new Pair<>(customer_2.getMeat(meatChannel, currencyChannel), distributor_1));    // 6
        products2.add(new Pair<>(customer_2.getMeat(meatChannel, currencyChannel), distributor_2));    // 7
        products2.add(new Pair<>(customer_2.getMeat(meatChannel, currencyChannel), distributor_2));    // 8
        products2.add(null);                                                                           // 9
        products2.add(new Pair<>(customer_2.getMeal(meatChannel, currencyChannel), restaurant_1));     //10
        products2.add(null);                                                                           //11
        products2.add(new Pair<>(customer_2.getMeat(meatChannel, currencyChannel), distributor_2));    //12

        ArrayList<TransactionCreator> distributors = new ArrayList<>();
        distributors.add(distributor_1);
        distributors.add(distributor_2);

        // Run the simulation:
        for (int i = 0; i < 30; ++i) {
            if (i < products1.size() && products1.get(i) != null) {
                customer_1.request(
                        productPrepareHelper.prepareProduct(products1.get(i).first, customer_1),
                        products1.get(i).first,
                        products1.get(i).second);
            } else if (i == products1.size() || i == products1.size() + 5) {
                // Double spending:
                products1.get(0).first.resetCounter();
                customer_1.request(
                        productPrepareHelper.prepareProduct(products1.get(0).first, customer_1),
                        products1.get(0).first,
                        products1.get(0).second);
            }
            if (i < products2.size() && products2.get(i) != null) {
                customer_2.request(
                        productPrepareHelper.prepareProduct(products2.get(i).first, customer_2),
                        products2.get(i).first,
                        products2.get(i).second);
            }
            simulation.nextTick();
        }

        // Prepare for report generating:
        ArrayList<Product> allProducts = new ArrayList<>();
        for (int i = 0; i < Integer.max(products1.size(), products2.size()); ++i) {
            if (i < products1.size() && products1.get(i) != null) {
                allProducts.add(products1.get(i).first);
            }
            if (i < products2.size() && products2.get(i) != null) {
                allProducts.add(products2.get(i).first);
            }
        }

        // Generate the reports:
        String foodChainReport = reportHandler.getFoodChainReport(allProducts);
        String doubleSpendingReport = reportHandler.getDoubleSpendingReport();
        String transactionReport = reportHandler.getTransactionReport();

        // Print the reports:
        logger.log(Level.INFO, "================================================================================================================");
        logger.log(Level.INFO, foodChainReport);
        logger.log(Level.INFO, "================================================================================================================");
        logger.log(Level.INFO, doubleSpendingReport);
        logger.log(Level.INFO, "================================================================================================================");
        logger.log(Level.INFO, transactionReport);

        // Save the reports:
        reportHandler.writeToFile(new HashMap<String, String>() {{
            put("food_chain_report_2.txt", foodChainReport);
            put("double_spending_report_2.txt", doubleSpendingReport);
            put("transaction_report_2.txt", transactionReport);
        }});


    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    public void case3() {
        // Could be in array, but I don't want to remember the indices :-)
        TransactionCreator customer_1 = factory.createCustomer();
        TransactionCreator customer_2 = factory.createCustomer();
        TransactionCreator breeder_1 = factory.createBreeder();
        TransactionCreator breeder_2 = factory.createBreeder();
        TransactionCreator grower_1 = factory.createGrower();
        TransactionCreator grower_2 = factory.createGrower();
        TransactionCreator butcher_1 = factory.createButcher();
        TransactionCreator butcher_2 = factory.createButcher();
        TransactionCreator juice_prod_1 = factory.createJuiceProducer();
        TransactionCreator juice_prod_2 = factory.createJuiceProducer();
        TransactionCreator oil_prod_1 = factory.createOilProducer();
        TransactionCreator oil_prod_2 = factory.createOilProducer();
        TransactionCreator milk_proc_1 = factory.createMilkProcessor();
        TransactionCreator milk_proc_2 = factory.createMilkProcessor();
        TransactionCreator pickler_1 = factory.createPickler();
        TransactionCreator pickler_2 = factory.createPickler();
        TransactionCreator restaurant_1 = factory.createRestaurant();
        TransactionCreator restaurant_2 = factory.createRestaurant();
        TransactionCreator distributor_1 = factory.createDistributor();
        TransactionCreator distributor_2 = factory.createDistributor();

        ArrayList<Product> products1 = new ArrayList<>();
        for (int i = 0; i < 5; ++i) {
            products1.add(customer_1.getMeat(meatChannel, currencyChannel));
            products1.add(customer_1.getMilk(milkChannel, currencyChannel));
            products1.add(customer_1.getCheese(milkChannel, currencyChannel));
            products1.add(customer_1.getOrange(cropChannel, currencyChannel));
            products1.add(customer_1.getApple(cropChannel, currencyChannel));
            products1.add(customer_1.getOlive(cropChannel, currencyChannel));
            products1.add(customer_1.getOrangeJuice(cropChannel, currencyChannel));
            products1.add(customer_1.getAppleJuice(cropChannel, currencyChannel));
            products1.add(customer_1.getOil(cropChannel, currencyChannel));
            products1.add(customer_1.getPickledOlive(cropChannel, currencyChannel));
            products1.add(customer_1.getMeal(meatChannel, currencyChannel));
        }

        ArrayList<Product> products2 = new ArrayList<>();
        for (int i = 0; i < 5; ++i) {
            products2.add(customer_2.getMeat(meatChannel, currencyChannel));
            products2.add(customer_2.getMilk(milkChannel, currencyChannel));
            products2.add(customer_2.getCheese(milkChannel, currencyChannel));
            products2.add(customer_2.getOrange(cropChannel, currencyChannel));
            products2.add(customer_2.getApple(cropChannel, currencyChannel));
            products2.add(customer_2.getOlive(cropChannel, currencyChannel));
            products2.add(customer_2.getOrangeJuice(cropChannel, currencyChannel));
            products2.add(customer_2.getAppleJuice(cropChannel, currencyChannel));
            products2.add(customer_2.getOil(cropChannel, currencyChannel));
            products2.add(customer_2.getPickledOlive(cropChannel, currencyChannel));
            products2.add(customer_2.getMeal(meatChannel, currencyChannel));
        }

        for (int i = 0; i < products1.size(); ++i) {
            Commodity money;
            List<Operation> ops = products1.get(i).getOperationList();
            money = productPrepareHelper.prepareProduct(products1.get(i), customer_1);
            if (i % 2 == 0) {
                if (ops.get(ops.size() - 1) == MakeMeal.getInstance()) {
                    customer_1.request(money, products1.get(i), restaurant_1);
                } else {
                    customer_1.request(money, products1.get(i), distributor_1);
                }
            } else {
                if (ops.get(ops.size() - 1) == MakeMeal.getInstance()) {
                    customer_1.request(money, products1.get(i), restaurant_2);
                } else {
                    customer_1.request(money, products1.get(i), distributor_2);
                }
            }
        }

        for (int i = 0; i < products2.size(); ++i) {
            Commodity money;
            List<Operation> ops = products2.get(i).getOperationList();
            money = productPrepareHelper.prepareProduct(products2.get(i), customer_2);
            if (i % 2 == 0) {
                if (ops.get(ops.size() - 1) == MakeMeal.getInstance()) {
                    customer_2.request(money, products2.get(i), restaurant_1);
                } else {
                    customer_2.request(money, products2.get(i), distributor_1);
                }
            } else {
                if (ops.get(ops.size() - 1) == MakeMeal.getInstance()) {
                    customer_2.request(money, products2.get(i), restaurant_2);
                } else {
                    customer_2.request(money, products2.get(i), distributor_2);
                }
            }
        }

        // Run the simulation
        simulation.runTicks(12);

        // Prepare for report generating:
        ArrayList<Product> allProducts = new ArrayList<>();
        allProducts.addAll(products1);
        allProducts.addAll(products2);

        // Generate the reports:
        String foodChainReport = reportHandler.getFoodChainReport(allProducts);
        String doubleSpendingReport = reportHandler.getDoubleSpendingReport();
        String transactionReport = reportHandler.getTransactionReport();

        // Print the reports:
        logger.log(Level.INFO, "================================================================================================================");
        logger.log(Level.INFO, foodChainReport);
        logger.log(Level.INFO, "================================================================================================================");
        logger.log(Level.INFO, doubleSpendingReport);
        logger.log(Level.INFO, "================================================================================================================");
        logger.log(Level.INFO, transactionReport);

        // Save the reports:
        reportHandler.writeToFile(new HashMap<String, String>() {{
            put("food_chain_report_3.txt", foodChainReport);
            put("double_spending_report_3.txt", doubleSpendingReport);
            put("transaction_report_3.txt", transactionReport);
        }});

    }
}



