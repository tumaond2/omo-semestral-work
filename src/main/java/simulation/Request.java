package simulation;

import channel.Channel;
import commodity.Commodity;
import operation.Operation;
import product.Product;
import transaction.Transaction;
import transactionCreator.TransactionCreator;
import transactionCreator.certificate.Certificate;
import transactionCreator.certificate.CertificateInvalidException;

public class Request {
    private Integer ticksLeft;
    private final Commodity commodity;
    private final Commodity money;

    private final Operation operation;
    private final TransactionCreator creator;

    private Request prerequisite;

    private final Product product;
    private final Simulation context;

    private Transaction commodityTransaction;
    private Transaction monetaryTransaction;

    @SuppressWarnings("SpellCheckingInspection")
    private Boolean publishCompletition = false;
    private Boolean isInvalid = false;

    public Request(Commodity commodity, Commodity money, Operation operation, TransactionCreator creator, Product product, Simulation context) {
        this.ticksLeft = operation.getTicks();
        this.commodity = commodity;
        this.operation = operation;
        this.creator = creator;
        this.product = product;
        this.context = context;
        this.money = money;
    }

    /**
     * Check if ticks have been decreased to 0
     *
     * @return ticks decremented to 0
     */
    private boolean isComplete() {
        return ticksLeft == 0;
    }

    public Integer getTicksLeft() {
        return ticksLeft;
    }

    public boolean prerequisiteNotFailed() {
        return (prerequisite == null || !prerequisite.isInvalid());
    }

    public void decreaseTickCount() {
        if (prerequisite != null && !prerequisite.isDone()) return;

        ticksLeft--;
    }

    public void phase2(Certificate certificate) {
        if (isComplete() && prerequisiteNotFailed()) {
            generateTransaction(certificate);
        }

        if (!prerequisiteNotFailed()) markInvalid();
    }

    public void phase3() {
        if (isComplete()) {
            markDone();
        }
    }

    public void setPrerequisite(Request prerequisite) {
        this.prerequisite = prerequisite;
    }

    public Operation getOperation() {
        return this.operation;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public Product getProduct() {
        return product;
    }

    private void markDone() {
        publishCompletition = true;
    }

    private void markInvalid() {
        isInvalid = true;
    }


    /**
     * Write transactions to both channels when this Request is fullfilled
     *
     * @param certificate Client certificate for "hashing"
     */
    @SuppressWarnings("SpellCheckingInspection")
    public void generateTransaction(Certificate certificate) {
        Transaction prevCommodityTransaction = prerequisite == null ? product.getInitTransaction() : prerequisite.getCommodityTransaction();
        Transaction prevMonetaryTransaction = prerequisite == null ? null : prerequisite.getMonetaryTransaction();
        commodityTransaction = new Transaction(creator, getCommodity(), getOperation(), context.getActualTick(), prevCommodityTransaction);
        monetaryTransaction = new Transaction(creator, money, getOperation(), context.getActualTick(), prevMonetaryTransaction);

        Boolean isCommodityDoubleSpended = prevCommodityTransaction != null && getAnnounceChannel().isDoubleSpending(prevCommodityTransaction);
        Boolean isCurrencyDoubleSpended = prevCommodityTransaction != null && getAnnounceChannel().isDoubleSpending(prevCommodityTransaction);

        if (isCommodityDoubleSpended || isCurrencyDoubleSpended) {
            markInvalid();
            System.out.println("DETECTED DOUBLESPENDING");
        }


        try {
            getAnnounceChannel().addTransaction(commodityTransaction, certificate);
            getMonetaryChannel().addTransaction(monetaryTransaction, certificate);
        } catch (CertificateInvalidException e) {
            System.out.println(" Certificate invalid ");
        } catch (Exception e) {
            System.out.println("Commodity creation failed");
            e.printStackTrace();
        }
    }

    public Transaction getCommodityTransaction() {
        return commodityTransaction;
    }


    public Transaction getMonetaryTransaction() {
        return monetaryTransaction;
    }

    public Channel getAnnounceChannel() {
        return product.getPrimaryChannel();
    }

    public Channel getMonetaryChannel() {
        return product.getSecondaryChannel();
    }

    /**
     * Check if Request has been released (in phase 3)
     *
     * @return request released
     */
    public boolean isDone() {
        return publishCompletition;
    }

    public Boolean isInvalid() {
        return isInvalid;
    }

}
