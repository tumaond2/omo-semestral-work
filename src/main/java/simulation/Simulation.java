package simulation;

import transactionCreator.TransactionCreator;

import java.util.ArrayList;
import java.util.List;

public class Simulation {

    Integer actualTick = 0;

    final List<TransactionCreator> creators = new ArrayList<>();

    public void subscribe(TransactionCreator creator) {
        creators.add(creator);
    }

    public void nextTick() {
        actualTick++;
        creators.forEach(TransactionCreator::nextTickPhase1);
        creators.forEach(TransactionCreator::nextTickPhase2);
        creators.forEach(TransactionCreator::nextTickPhase3);
    }

    public Integer getActualTick() {
        return actualTick;
    }


    public void runTicks(Integer n) {
        for (int i = 0; i < n; i++) {
            nextTick();
        }
    }

    public List<TransactionCreator> getCreators() {
        return creators;
    }
}
