package report;

import channel.Channel;
import commodity.Commodity;
import commodity.Currency;
import product.Product;
import simulation.Simulation;
import transaction.Transaction;
import transactionCreator.TransactionCreator;

import java.io.FileWriter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class is meant to take care of generating the reports for finished Simulation and of writing them into the file/files.
 * The Simulation for which the reports should be generated is needed as a parameter for the constructor.
 * To work correctly, the channels containing the transactions has to be provided.
 * Use the method addChannel(Channel ch) to add individual channels and removeChannel(Channel ch) to remove them.
 */
@SuppressWarnings("StringBufferReplaceableByString")
public class ReportHandler {
    private Simulation context;
    private final Set<Channel> channels;

    public ReportHandler(Simulation simulation) {
        if (simulation == null) {
            throw new NullPointerException();
        } else {
            this.context = simulation;
            this.channels = new HashSet<>();
        }
    }

    public void setContext(Simulation simulation) {
        if (simulation != null) {
            this.context = simulation;
        }
    }

    public void addChannel(Channel ch) {
        channels.add(ch);
    }

    public void removeChannel(Channel ch) {
        channels.remove(ch);
    }

    /**
     * Creates a String containing Food Chain report for Product product.
     *
     * @param product Product for which to generate report
     * @return the generated report in a String
     */
    public String getFoodChainReport(Product product) {
        StringBuilder builder = new StringBuilder();
        builder.append("Food Chain Report: -----------------------------------------------------------------------------------------------------\n")
                .append("For product ")
                .append(product)
                .append(" in ")
                .append(product.getPrimaryChannel().getFoodChainReport(product.getCommodity()));
        return builder.toString();
    }

    /**
     * Creates a String containing Food Chain report for each Product in Collection of products.
     *
     * @param products Collection of Product
     * @return the generated report in a String
     */
    public String getFoodChainReport(Collection<Product> products) {
        StringBuilder builder = new StringBuilder();
        for (Product p : products) {
            builder.append(getFoodChainReport(p));
            builder.append("\n");
        }
        return builder.toString();
    }

    /**
     * Creates a String containing Double Spending report.
     *
     * @return the generated report in a String
     */
    public String getDoubleSpendingReport() {
        StringBuilder builder = new StringBuilder();
        builder.append("Double Spending Report: ------------------------------------------------------------------------------------------------\n");

        for (Channel ch : channels) {
            builder.append(ch.getDoubleSpendingReport());
        }

        return builder.toString();
    }

    /**
     * Creates a String containing Transaction report.
     *
     * @return the generated report in a String
     */
    public String getTransactionReport() {
        StringBuilder builder = new StringBuilder();
        List<Map<TransactionCreator, Set<Commodity>>> commoditiesOwnershipByTick = this.getCommoditiesOwnershipByTick();
        builder.append("Transaction Report: ----------------------------------------------------------------------------------------------------\n");
        HashMap<TransactionCreator, Integer> money = new HashMap<>();
        for (TransactionCreator tc : context.getCreators()) {
            money.put(tc, 0);
        }

        Map<Integer, List<Transaction>> transactionsByTick = channels.stream()
                .map(ch -> ch.getTransactionPool().getTransactionList())
                .flatMap(List::stream)
                .collect(Collectors.toList())
                .stream().collect(Collectors.groupingBy(Transaction::getTick));

        for (Integer i = 0; i < context.getActualTick() + 1; ++i) {
            builder
                    .append("Tick ")
                    .append(i)
                    .append(":\n");

            // Because each operation is in two transactions (in its commodity channel and in currency channel)
            // it is necessary to compute the gain and then divide it by 2. Then it can be added to total amount
            // owned by the TransactionCreator
            HashMap<TransactionCreator, Integer> money_gain = new HashMap<>();
            for (TransactionCreator tc : context.getCreators()) {
                money_gain.put(tc, 0);
            }

            List<Transaction> l;
            if (transactionsByTick.containsKey(i)) {
                l = transactionsByTick.get(i);
                for (Transaction t : l) {

                    builder
                            .append("\t")
                            .append(t.toSimpleString());

                    Integer gain = t.getOperation() == null ? 0 : t.getOperation().getPrice();
                    money_gain.put(t.getOrigin(), money_gain.get(t.getOrigin()) + gain);
                }
            } else {
                builder.append("\tNo transaction in this tick.\n");
            }
            builder.append("\n");
            for (TransactionCreator tc : money.keySet()) {
                money.put(tc, money.get(tc) + money_gain.get(tc) / 2);
                builder
                        .append("\t")
                        .append(tc)
                        .append(" currency amount: ")
                        .append(money.get(tc))
                        .append("\n");
            }
            builder.append("\n");
            for (TransactionCreator tc : commoditiesOwnershipByTick.get(i).keySet()) {
                builder.append("\t")
                        .append(tc);
                if (commoditiesOwnershipByTick.get(i).get(tc).isEmpty()) {
                    builder.append(" has NO commodities.\n");
                } else {
                    builder.append(" has commodities: ");
                    for (Commodity c : commoditiesOwnershipByTick.get(i).get(tc)) {
                        builder
                                .append(c)
                                .append(" | ");
                    }
                    builder.append("\n");
                }
            }
            builder.append("\n");

        }

        return builder.toString();
    }

    /**
     * Method writes the content to files (and creates them if needed) based on the input.
     *
     * @param outputMap contains names of files to write to as keys
     *                  and content which will be written to relevant file as values.
     */
    public void writeToFile(Map<String, String> outputMap) {
        for (String filename : outputMap.keySet()) {
            writeToFile(filename, outputMap.get(filename));
        }
    }

    /**
     * Method writes the content to file (and creates it if needed) based on the input.
     *
     * @param filename String which determines the name of the file.
     * @param content  String which determines the content which will be written to the file.
     */
    public void writeToFile(String filename, String content) {
        try {
            FileWriter report = new FileWriter(filename);
            report.write(content);
            report.close();
        } catch (Exception e) {
            System.out.println("! ERROR: IOException while making a report to file '" + filename + "'");
        }
    }


    /**
     * Method used in getTransactionReport(). It generates a List which on i-th index contains information
     * about ownership of commodities. The information are in a Map, where key is TransactionCreator and the
     * relevant value is a List of Commodities, which is owned by the TransactionCreator in the i-th tick.
     */
    private List<Map<TransactionCreator, Set<Commodity>>> getCommoditiesOwnershipByTick() {
        List<Map<TransactionCreator, Set<Commodity>>> ret = new ArrayList<>();
        for (Integer tick = 0; tick < context.getActualTick() + 1; ++tick) {
            Map<TransactionCreator, Set<Commodity>> map = new HashMap<>();
            for (TransactionCreator tc : context.getCreators()) {
                map.put(tc, new HashSet<>());
            }
            ret.add(map);
        }

        List<Commodity> commodities = channels.stream()
                .map(ch -> ch.getTransactionPool().getTransactionList())
                .flatMap(List::stream)
                .map(Transaction::getCommodity)
                .distinct()
                .collect(Collectors.toList());

        Map<Commodity, Integer> com_last_seen = new HashMap<>();
        for (Commodity c : commodities) {
            if (!(c instanceof Currency)) {
                com_last_seen.put(c, 0);
            }
        }
        commodities.clear();

        Map<Integer, List<Transaction>> transactionsByTick = channels.stream()
                .map(ch -> ch.getTransactionPool().getTransactionList())
                .flatMap(List::stream)
                .collect(Collectors.toList())
                .stream().collect(Collectors.groupingBy(Transaction::getTick));

        // Would you like some spaghetti?
        for (Integer tick = 1; tick < context.getActualTick() + 1; ++tick) {
            List<Transaction> transactions = transactionsByTick.get(tick);
            if (transactions != null && !transactions.isEmpty()) {
                for (Transaction t : transactions) {
                    for (Commodity c : com_last_seen.keySet()) {
                        if (t.getCommodity().equals(c)) {
                            int i = tick;
                            while (i > com_last_seen.get(c)) {
                                ret.get(i).get(t.getOrigin()).add(c);
                                --i;
                            }
                            com_last_seen.put(c, tick);
                        }
                    }
                }
            }
        }

        return ret;
    }
}
