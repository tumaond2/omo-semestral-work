package channel;

import commodity.Commodity;
import transaction.Transaction;
import transaction.TransactionPool;
import transactionCreator.certificate.Certificate;
import transactionCreator.certificate.CertificateInvalidException;

@SuppressWarnings("StringBufferReplaceableByString")
public abstract class Channel {
    final TransactionPool transactionPool = new TransactionPool();

    /**
     * Add transaction to the transaction pool
     *
     * @param transaction the transaction
     * @param certificate signee certificate
     * @throws CertificateInvalidException Certificate may be invalid
     */
    public void addTransaction(Transaction transaction, Certificate certificate) throws CertificateInvalidException {
        transactionPool.addTransaction(transaction, certificate);
    }

    /**
     * Return true if transaction would create double spending
     *
     * @param transaction the transaction
     * @return the result
     */
    public Boolean isDoubleSpending(Transaction transaction) {
        return transactionPool.isDoubleSpending(transaction);
    }


    public TransactionPool getTransactionPool() {
        return transactionPool;
    }

    /**
     * Generates log for whole channel
     *
     * @return the log string
     */
    public String getLog() {
        StringBuilder builder = new StringBuilder();
        builder
                .append("\n")
                .append("LOG for: ")
                .append(getClass().getName()).append(" -------------------------------------------------------\n")
                .append(transactionPool.getCompleteLog());

        return builder.toString();
    }

    /**
     * Generate report for single commodity
     *
     * @param commodity commodity for which to generate report
     * @return string with the report
     */
    public String getFoodChainReport(Commodity commodity) {
        StringBuilder builder = new StringBuilder();
        builder
                .append("Channel ")
                .append(getClass().getName())
                .append(" -------------------------------------------------------\n")
                .append(transactionPool.getFoodChainReport(commodity));

        return builder.toString();
    }

    /**
     * Generate transaction report
     *
     * @return the report
     */
    public String getTransactionReport() {
        StringBuilder builder = new StringBuilder();
        builder
                .append("\n")
                .append("Transaction report for: ")
                .append(getClass().getName())
                .append(" -------------------------------------------------------\n")
                .append(transactionPool.getTransactionReport());

        return builder.toString();
    }

    /**
     * Generate list of double spending transactions
     *
     * @return string with the report
     */
    public String getDoubleSpendingReport() {
        StringBuilder builder = new StringBuilder();
        builder.append("\nDouble Spending Detection for: ")
                .append(getClass().getName())
                .append(" -------------------------------------------------------\n");
        String doubleSpendingString = transactionPool.getDoubleSpendingReport();
        if (doubleSpendingString.length() == 0) {
            builder.append("No double spending detected.\n");
        } else {
            builder.append(doubleSpendingString);
        }
        return builder.toString();
    }
}
