package testHelpers;

@SuppressWarnings("CanBeFinal")
public class Pair<U, V> {
    public U first;
    public V second;

    public Pair() {
        this.first = null;
        this.second = null;
    }

    public Pair(U first, V second) {
        this.first = first;
        this.second = second;
    }
}
