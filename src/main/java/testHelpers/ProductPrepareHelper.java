package testHelpers;

import channel.Channel;
import commodity.Commodity;
import operation.CreateCurrency;
import product.Product;
import transactionCreator.TransactionCreator;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("FieldCanBeLocal")
public class ProductPrepareHelper {

    private final HashMap<String, Channel> channels;

    private static final Logger LOG = Logger.getLogger(ProductPrepareHelper.class.getName());

    public ProductPrepareHelper(HashMap<String, Channel> channels) {
        this.channels = channels;
    }

    /**
     * Takes a Product and returns Commodity Currency, which is the exact amount of the product cost.
     *
     * @param p        product
     * @param customer customer preparing the product
     * @return the commodity
     */
    public Commodity prepareProduct(Product p, TransactionCreator customer) {
        Commodity money = null;
        try {
            money = CreateCurrency.getInstance().apply(null, customer);
            money.setValue(p.getPrice());
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Customer cannot create new money");
        }

        return money;
    }

}
