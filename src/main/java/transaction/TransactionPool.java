package transaction;

import commodity.Commodity;
import transactionCreator.TransactionCreator;
import transactionCreator.certificate.Certificate;
import transactionCreator.certificate.CertificateInvalidException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"SameReturnValue", "SpellCheckingInspection"})
public class TransactionPool {
    final List<Transaction> transactionList;

    public TransactionPool() {
        transactionList = new ArrayList<>();
    }

    /**
     * Add Transaction to the transaction Pool.
     * Client is able to add any transaction, as long as it is signed with the right Cetrificate.
     * Transactions in the TransactionPool may not be correct. The client pulling them out is responsible for filtering only the valid ones.
     * This should mimick the behavior of the Ledger, since not all Ledger blocks have to be valid.
     *
     * @param transaction Transaction to be added
     * @param certificate Certificate of the client adding the transaction
     * @throws CertificateInvalidException If client is trying to add transaction under someone elses name
     */
    public void addTransaction(Transaction transaction, Certificate certificate) throws CertificateInvalidException {
        if (!transaction.isCertificateValid(certificate)) throw new CertificateInvalidException();

        transactionList.add(transaction);
    }

    /**
     * Filter transactions for commodity.
     * Using "==" in the filter intentionally, as ".equals" would only select Single commodity instance. Aka. with the same hash.
     * "==" should filter all occurences of given Commodity instances based on the pointer value.
     *
     * @param commodity target Commodity instance
     * @return Stream of all transactions for given commodity instance
     */
    private Stream<Transaction> getTransactionsForCommodity(Commodity commodity) {
        return transactionList.stream()
                .filter(transaction -> transaction.getCommodity() == commodity);
    }

    public boolean isDoubleSpending(Transaction transaction) {
        return transactionList.stream().anyMatch(t -> t.getPrevious() == transaction);
    }

    /**
     * Reduce all operations for given Commodity and check if the last state hash equals the current commodity hash
     *
     * @param commodity current state of commodity
     * @return whether commodity is valid
     */
    public boolean isCommodityStateValid(Commodity commodity) {
        return true;
    }

    public String getCompleteLog() {
        StringBuilder builder = new StringBuilder();
        transactionList.forEach(t -> builder.append(t).append("\n"));
        return builder.toString();
    }

    public String getFoodChainReport(Commodity commodity) {
        StringBuilder builder = new StringBuilder();

        builder.append("Printing transaction pool for ").append(commodity).append("\n");
        Supplier<Stream<Transaction>> transactions = () -> transactionList.stream().filter(t -> t.getCommodity() == commodity);

        transactions.get().forEach(t -> builder.append(t).append("\n"));

        Commodity finalState = transactions.get()
                .reduce(null,
                        (result, t) -> t.getOperation().apply(result, t.getOrigin()),
                        (result, t) -> result);

        builder.append("Final state: ")
                .append(finalState)
                .append("\n");

        return builder.toString();
    }

    public String getTransactionReport() {
        StringBuilder builder = new StringBuilder();
        Supplier<Map<Integer, List<Transaction>>> transactions = () -> transactionList.stream().collect(Collectors.groupingBy(Transaction::getTick));

        transactions.get().forEach((key1, value1) -> {
            builder
                    .append("Tick: ")
                    .append(key1)
                    .append("\n");
            Supplier<Map<TransactionCreator, List<Transaction>>> creatorTransactions = () -> value1.stream().collect(Collectors.groupingBy(Transaction::getOrigin));
            creatorTransactions.get().forEach((key, value) -> builder
                    .append(key.hashCode())
                    .append(" ")
                    .append("Number of transactions: ")
                    .append(value.size())
                    .append("\n"));


        });

        return builder.toString();
    }

    // Double spending detect:
    public String getDoubleSpendingReport() {
        StringBuilder builder = new StringBuilder();
        Map<Transaction, List<Transaction>> doubleSpendingTransaction = transactionList.stream()
                .filter(t -> t.getPrevious() != null)
                .collect(Collectors.groupingBy(Transaction::getPrevious))
                .entrySet().stream()
                .filter(e -> e.getValue().size() > 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        for (Map.Entry<Transaction, List<Transaction>> e : doubleSpendingTransaction.entrySet()) {
            builder.append("For TransactionCreator: ").append(e.getKey().getOrigin()).append(" detected ").append(e.getValue().size() - 1).append(" times.\n");
        }

        return builder.toString();
    }

    public List<Transaction> getTransactionList() {
        return this.transactionList;
    }

}
