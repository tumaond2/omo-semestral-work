package transaction;

import commodity.Commodity;
import operation.Operation;
import transactionCreator.TransactionCreator;
import transactionCreator.certificate.Certificate;

public class Transaction {
    private final TransactionCreator origin;
    private final Commodity commodity;
    private final Operation operation;
    private final Integer tick;
    private final Transaction previous;

    /**
     * @param origin     The initiator of the transaction
     * @param commodity  The resource to be modified by this transaction
     * @param operation  The operation to be performed on given resource
     * @param actualTick current tick in the simulation
     * @param previous   previous transaction reference
     */
    public Transaction(TransactionCreator origin, Commodity commodity, Operation operation, Integer actualTick, Transaction previous) {
        this.origin = origin;
        this.commodity = commodity;
        this.tick = actualTick;
        this.operation = operation;
        this.previous = previous;
    }

    public boolean isCertificateValid(Certificate certificate) {
        return origin.isCertificateValid(certificate);
    }

    public TransactionCreator getOrigin() {
        return origin;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public Operation getOperation() {
        return operation;
    }

    public Integer getTick() {
        return tick;
    }

    public String toString() {
        return "Transaction hash: " + hashCode() + " from " + origin + " on " + commodity
                + "operation: " + operation + "\n\tlength of operation: " + operation.getTicks()
                + " ticks\n\tfinished at tick: " + tick
                + "\n\tcost of operation: " + operation.getPrice()
                + "\n\tprevious Transaction: " + (previous == null ? "null" : previous.hashCode());
    }

    public String toSimpleString() {
        return "Transaction hash: " + hashCode() + " from " + origin + " on " + commodity + "\n";
    }

    public Transaction getPrevious() {
        return previous;
    }
}
