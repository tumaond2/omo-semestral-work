package product;

import channel.Channel;
import commodity.Commodity;
import operation.Operation;
import transaction.Transaction;
import transactionCreator.certificate.Certificate;
import transactionCreator.certificate.CertificateInvalidException;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Product {
    final Commodity commodity;
    final List<Operation> operationList;
    final String name;
    Integer currentIndex;
    final Integer price;

    final Channel primaryChannel;
    final Channel secondaryChannel;

    Transaction initTransaction;

    private static final Logger LOG = Logger.getLogger(Product.class.getName());

    public Product(Commodity commodity, List<Operation> operationList, String name, Integer price, Channel primaryChannel, Channel secondaryChannel) {
        this.commodity = commodity;
        this.operationList = operationList;
        this.name = name;
        currentIndex = operationList.size() - 1;
        this.price = price;
        this.primaryChannel = primaryChannel;
        this.secondaryChannel = secondaryChannel;
    }

    public List<Operation> getOperationList() {
        return operationList;
    }

    public Operation getNextOperation() {
        if (currentIndex < 0) return null;

        return operationList.get(currentIndex);
    }

    public void validateOperationRemoval() {
        currentIndex--;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public Channel getPrimaryChannel() {
        return primaryChannel;
    }

    public void addPrimaryTransaction(Transaction transaction, Certificate certificate) throws CertificateInvalidException {
        this.getPrimaryChannel().addTransaction(transaction, certificate);
    }

    public Channel getSecondaryChannel() {
        return secondaryChannel;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public Integer getPrice() {
        return price;
    }

    public void setInitTransaction(Transaction init) {
        this.initTransaction = init;
    }

    public Transaction getInitTransaction() {
        return initTransaction;
    }

    /**
     * Only ever to be done for testing if double spending detection works
     */
    @SuppressWarnings("StringBufferReplaceableByString")
    public void resetCounter() {
        currentIndex = operationList.size() - 1;
        StringBuilder builder = new StringBuilder();
        builder.append("==================================================================================\n")
                .append("DANGER ZONE: resetting counter\n")
                .append("Only do this for testing double spending\n")
                .append("==================================================================================");
        LOG.log(Level.WARNING, builder.toString());
    }
}
