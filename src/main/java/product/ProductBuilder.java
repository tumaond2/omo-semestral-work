package product;

import channel.Channel;
import commodity.Commodity;
import operation.Initialize;
import operation.Operation;
import transaction.Transaction;
import transactionCreator.TransactionCreator;
import transactionCreator.certificate.Certificate;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductBuilder {

    Commodity commodity = null;
    final List<Operation> operations = new ArrayList<>();
    String name = null;

    Channel primaryChannel = null;
    Channel secondaryChannel = null;

    private static final Logger LOG = Logger.getLogger(ProductBuilder.class.getName());

    public ProductBuilder setCommodity(Commodity commodity) {
        this.commodity = commodity;

        return this;
    }

    public ProductBuilder addOperation(Operation operation) {
        this.operations.add(operation);

        return this;
    }

    public ProductBuilder setPrimaryChannel(Channel channel) {
        this.primaryChannel = channel;
        return this;
    }

    public ProductBuilder setSecondaryChannel(Channel channel) {
        this.secondaryChannel = channel;
        return this;
    }

    public ProductBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public Product build(TransactionCreator creator, Certificate certificate) {
        Product prod = new Product(commodity, operations, name, operations.stream().map(Operation::getPrice).reduce(0, Integer::sum), primaryChannel, secondaryChannel);
        Transaction transaction = new Transaction(creator, commodity, Initialize.getInstance(), 0, null);

        try {
            prod.addPrimaryTransaction(transaction, certificate);
            prod.setInitTransaction(transaction);
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Product could not be created");
        }

        return prod;
    }
}
