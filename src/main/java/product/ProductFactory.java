package product;

import channel.Channel;
import commodity.*;
import operation.*;
import transactionCreator.TransactionCreator;
import transactionCreator.certificate.Certificate;

public class ProductFactory {
    static Integer meatCount = 0;
    static Integer milkCount = 0;
    static Integer cheeseCount = 0;
    static Integer orangeCount = 0;
    static Integer appleCount = 0;
    static Integer oliveCount = 0;

    public static Product getMeat(TransactionCreator creator, Certificate certificate, Channel primaryChannel, Channel secondaryChannel) {
        return new ProductBuilder()
                .setCommodity(new Meat())
                .addOperation(CreateMeat.getInstance())
                .addOperation(ProcessMeat.getInstance())
                .addOperation(DistributeMeat.getInstance())
                .setName("Meat" + meatCount++)
                .setPrimaryChannel(primaryChannel)
                .setSecondaryChannel(secondaryChannel)
                .build(creator, certificate);
    }

    public static Product getMilk(TransactionCreator creator, Certificate certificate, Channel primaryChannel, Channel secondaryChannel) {
        return new ProductBuilder()
                .setCommodity(new Milk())
                .addOperation(CreateMilk.getInstance())
                .addOperation(DistributeMilkProduct.getInstance())
                .setName("Milk" + milkCount++)
                .setPrimaryChannel(primaryChannel)
                .setSecondaryChannel(secondaryChannel)
                .build(creator, certificate);
    }

    public static Product getCheese(TransactionCreator creator, Certificate certificate, Channel primaryChannel, Channel secondaryChannel) {
        return new ProductBuilder()
                .setCommodity(new Milk())
                .addOperation(CreateMilk.getInstance())
                .addOperation(MakeCheese.getInstance())
                .addOperation(DistributeMilkProduct.getInstance())
                .setName("Cheese" + cheeseCount++)
                .setPrimaryChannel(primaryChannel)
                .setSecondaryChannel(secondaryChannel)
                .build(creator, certificate);
    }

    public static Product getOrange(TransactionCreator creator, Certificate certificate, Channel primaryChannel, Channel secondaryChannel) {
        return new ProductBuilder()
                .setCommodity(new Orange())
                .addOperation(CreateOrange.getInstance())
                .addOperation(DistributeOrangeProduct.getInstance())
                .setName("Orange" + orangeCount++)
                .setPrimaryChannel(primaryChannel)
                .setSecondaryChannel(secondaryChannel)
                .build(creator, certificate);
    }

    public static Product getOrangeJuice(TransactionCreator creator, Certificate certificate, Channel primaryChannel, Channel secondaryChannel) {
        return new ProductBuilder()
                .setCommodity(new Orange())
                .addOperation(CreateOrange.getInstance())
                .addOperation(MakeOrangeJuice.getInstance())
                .addOperation(DistributeOrangeProduct.getInstance())
                .setName("OrangeJuice" + orangeCount++)
                .setPrimaryChannel(primaryChannel)
                .setSecondaryChannel(secondaryChannel)
                .build(creator, certificate);
    }

    public static Product getApple(TransactionCreator creator, Certificate certificate, Channel primaryChannel, Channel secondaryChannel) {
        return new ProductBuilder()
                .setCommodity(new Apple())
                .addOperation(CreateApple.getInstance())
                .addOperation(DistributeAppleProduct.getInstance())
                .setName("Apple" + appleCount++)
                .setPrimaryChannel(primaryChannel)
                .setSecondaryChannel(secondaryChannel)
                .build(creator, certificate);
    }

    public static Product getAppleJuice(TransactionCreator creator, Certificate certificate, Channel primaryChannel, Channel secondaryChannel) {
        return new ProductBuilder()
                .setCommodity(new Apple())
                .addOperation(CreateApple.getInstance())
                .addOperation(MakeAppleJuice.getInstance())
                .addOperation(DistributeAppleProduct.getInstance())
                .setName("AppleJuice" + appleCount++)
                .setPrimaryChannel(primaryChannel)
                .setSecondaryChannel(secondaryChannel)
                .build(creator, certificate);
    }

    public static Product getOlive(TransactionCreator creator, Certificate certificate, Channel primaryChannel, Channel secondaryChannel) {
        return new ProductBuilder()
                .setCommodity(new Olive())
                .addOperation(CreateOlive.getInstance())
                .addOperation(DistributeOliveProduct.getInstance())
                .setName("Olive" + oliveCount++)
                .setPrimaryChannel(primaryChannel)
                .setSecondaryChannel(secondaryChannel)
                .build(creator, certificate);
    }

    public static Product getOil(TransactionCreator creator, Certificate certificate, Channel primaryChannel, Channel secondaryChannel) {
        return new ProductBuilder()
                .setCommodity(new Olive())
                .addOperation(CreateOlive.getInstance())
                .addOperation(MakeOil.getInstance())
                .addOperation(DistributeOliveProduct.getInstance())
                .setName("OliveOil" + oliveCount++)
                .setPrimaryChannel(primaryChannel)
                .setSecondaryChannel(secondaryChannel)
                .build(creator, certificate);
    }

    public static Product getMeal(TransactionCreator creator, Certificate certificate, Channel primaryChannel, Channel secondaryChannel) {
        return new ProductBuilder()
                .setCommodity(new Meat())
                .addOperation(CreateMeat.getInstance())
                .addOperation(ProcessMeat.getInstance())
                .addOperation(DistributeMeat.getInstance())
                .addOperation(MakeMeal.getInstance())
                .setName("Meal" + meatCount++)
                .setPrimaryChannel(primaryChannel)
                .setSecondaryChannel(secondaryChannel)
                .build(creator, certificate);

    }


    public static Product getPickledOlive(TransactionCreator creator, Certificate certificate, Channel primaryChannel, Channel secondaryChannel) {
        return new ProductBuilder()
                .setCommodity(new Olive())
                .addOperation(CreateOlive.getInstance())
                .addOperation(PickleOlive.getInstance())
                .addOperation(DistributeOliveProduct.getInstance())
                .setName("Pickled_Olive" + oliveCount++)
                .setPrimaryChannel(primaryChannel)
                .setSecondaryChannel(secondaryChannel)
                .build(creator, certificate);
    }
}
