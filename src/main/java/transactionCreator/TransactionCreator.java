package transactionCreator;

import channel.Channel;
import commodity.Commodity;
import operation.Operation;
import product.Product;
import product.ProductFactory;
import simulation.Request;
import simulation.Simulation;
import transactionCreator.certificate.Certificate;
import transactionCreator.exceptions.CannotProvideGivenCommodityException;
import transactionCreator.exceptions.SourceCommodityNotValid;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class TransactionCreator {
    final Certificate certificate;
    Simulation context = null;
    String name = null;
    @SuppressWarnings("UnusedAssignment")
    Random random = null;

    final List<Operation> supportedOperations = new ArrayList<>();

    // I requested from others
    final List<Request> awaitingRequests = new ArrayList<>();

    // They requested from me
    List<Request> myRequests = new ArrayList<>();

    private static final Logger LOG = Logger.getLogger(TransactionCreator.class.getName());

    public TransactionCreator() {
        this.certificate = new Certificate();
        this.random = new Random();
    }

    protected void addOperation(Operation operation) {
        if (operation == null) throw new NullPointerException();

        supportedOperations.add(operation);
        operation.addCreator(this);
    }

    protected void addContext(Simulation simulation) {
        if (simulation == null) throw new NullPointerException();
        this.context = simulation;
        this.context.subscribe(this);
    }

    protected void setName(String name) {
        if (name == null) throw new NullPointerException();

        this.name = name;
    }


    public boolean isCertificateValid(Certificate cert) {
        return certificate.equals(cert);
    }

    /**
     * Request someone to produce commodity
     * Should check whether has the last operation in the list available.
     * If so, find someone who can do the previous operation in chain. Check that source matches in transactionList
     *
     * @param source  Commodity that I am providing for the expected outcome
     * @param product Defined product
     * @return ticks when to expect outcome
     * @throws CannotProvideGivenCommodityException if cannot provide given commodity
     * @throws SourceCommodityNotValid              if source is not valid
     */
    protected Request requestCommodity(Commodity source, Product product) throws CannotProvideGivenCommodityException, SourceCommodityNotValid {
        Operation nextOperation = product.getNextOperation();
        if (!supportedOperations.contains(nextOperation)) throw new CannotProvideGivenCommodityException();
        if (!nextOperation.canBeApplied(product.getCommodity(), source, this)) throw new SourceCommodityNotValid();
        source = nextOperation.decreaseValue(source);

        Request future = new Request(product.getCommodity(), source, nextOperation, this, product, context);
        myRequests.add(future);
        product.validateOperationRemoval();

        if (product.getNextOperation() != null) {
            try {
                TransactionCreator next = product
                        .getNextOperation()
                        .getTransactionCreators()
                        .get(random.nextInt(product.getNextOperation().getTransactionCreators().size()));
                future.setPrerequisite(next.requestCommodity(source, product));
                LOG.log(Level.INFO, this + " Passing request " + product.toString() + " To " + next);
            } catch (Exception e) {
                LOG.log(Level.WARNING, this + " Reverting");
                nextOperation.increaseValue(source);
                myRequests.remove(future);
                throw e;
            }
        }

        return future;
    }

    public void request(Commodity source, Product product, TransactionCreator from) {
        try {
            LOG.log(Level.INFO,
                    this + " Requesting " + product.toString() + " From " + from + " in exchange for " + source);
            Request future = from.requestCommodity(source, product);
            this.awaitingRequests.add(future);
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Provider cannot provide " + product.toString());
        }
    }

    /**
     * At start of each Tick
     * <p>
     * Process requests currently waiting for me
     */
    public void nextTickPhase1() {
        myRequests.forEach(Request::decreaseTickCount);
    }

    /**
     * Process requests that matured in current tick
     */
    public void nextTickPhase2() {
        myRequests.forEach(r -> r.phase2(certificate));
    }

    /**
     * At end of each Tick
     * <p>
     * Release requests done in current simulation tick
     */
    public void nextTickPhase3() {
        myRequests.forEach(Request::phase3);
        myRequests = myRequests.stream().filter(r -> !r.isDone()).collect(Collectors.toList());
    }

    public String toString() {
        return this.name;
    }


    public Product getMeat(Channel primaryChannel, Channel secondaryChannel) {
        return ProductFactory.getMeat(this, certificate, primaryChannel, secondaryChannel);
    }

    public Product getMilk(Channel primaryChannel, Channel secondaryChannel) {
        return ProductFactory.getMilk(this, certificate, primaryChannel, secondaryChannel);
    }

    public Product getCheese(Channel primaryChannel, Channel secondaryChannel) {
        return ProductFactory.getCheese(this, certificate, primaryChannel, secondaryChannel);
    }

    public Product getOrange(Channel primaryChannel, Channel secondaryChannel) {
        return ProductFactory.getOrange(this, certificate, primaryChannel, secondaryChannel);
    }

    public Product getOrangeJuice(Channel primaryChannel, Channel secondaryChannel) {
        return ProductFactory.getOrangeJuice(this, certificate, primaryChannel, secondaryChannel);
    }

    public Product getApple(Channel primaryChannel, Channel secondaryChannel) {
        return ProductFactory.getApple(this, certificate, primaryChannel, secondaryChannel);
    }

    public Product getAppleJuice(Channel primaryChannel, Channel secondaryChannel) {
        return ProductFactory.getAppleJuice(this, certificate, primaryChannel, secondaryChannel);
    }

    public Product getOlive(Channel primaryChannel, Channel secondaryChannel) {
        return ProductFactory.getOlive(this, certificate, primaryChannel, secondaryChannel);
    }

    public Product getOil(Channel primaryChannel, Channel secondaryChannel) {
        return ProductFactory.getOil(this, certificate, primaryChannel, secondaryChannel);
    }

    public Product getPickledOlive(Channel primaryChannel, Channel secondaryChannel) {
        return ProductFactory.getPickledOlive(this, certificate, primaryChannel, secondaryChannel);
    }

    public Product getMeal(Channel primaryChannel, Channel secondaryChannel) {
        return ProductFactory.getMeal(this, certificate, primaryChannel, secondaryChannel);
    }
}
