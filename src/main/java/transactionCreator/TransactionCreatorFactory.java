package transactionCreator;

import operation.*;
import simulation.Simulation;

@SuppressWarnings("SpellCheckingInspection")
public class TransactionCreatorFactory {

    @SuppressWarnings("UnusedAssignment")
    Simulation simulation = null;
    Integer breederCount = 0;
    Integer growerCount = 0;
    Integer butcherCount = 0;
    Integer juiceProducerCount = 0;
    Integer oilProducerCount = 0;
    Integer milkProcessorCount = 0;
    Integer picklerCount = 0;
    Integer distributorCount = 0;
    Integer restaurantsCount = 0;
    Integer customerCount = 0;

    public TransactionCreatorFactory(Simulation context) {
        if (context == null) throw new NullPointerException();
        simulation = context;
    }

    public TransactionCreator createBreeder() {
        TransactionCreatorBuilder builder = new TransactionCreatorBuilder();

        return builder
                .addOperation(CreateMeat.getInstance())
                .addOperation(CreateMilk.getInstance())
                .addContext(simulation)
                .setName("Breeder" + breederCount++)
                .build();
    }

    public TransactionCreator createButcher() {
        TransactionCreatorBuilder builder = new TransactionCreatorBuilder();

        return builder
                .addOperation(ProcessMeat.getInstance())
                .addContext(simulation)
                .setName("Butcher" + butcherCount++)
                .build();
    }

    public TransactionCreator createCustomer() {
        TransactionCreatorBuilder builder = new TransactionCreatorBuilder();

        return builder
                .addOperation(CreateCurrency.getInstance())
                .addContext(simulation)
                .setName("Customer" + customerCount++)
                .build();
    }

    public TransactionCreator createGrower() {
        TransactionCreatorBuilder builder = new TransactionCreatorBuilder();

        return builder
                .addOperation(CreateOrange.getInstance())
                .addOperation(CreateApple.getInstance())
                .addOperation(CreateOlive.getInstance())
                .addContext(simulation)
                .setName("Grower" + growerCount++)
                .build();
    }

    public TransactionCreator createJuiceProducer() {
        TransactionCreatorBuilder builder = new TransactionCreatorBuilder();

        return builder
                .addOperation(MakeOrangeJuice.getInstance())
                .addOperation(MakeAppleJuice.getInstance())
                .addContext(simulation)
                .setName("Juice_Producer" + juiceProducerCount++)
                .build();
    }

    public TransactionCreator createMilkProcessor() {
        TransactionCreatorBuilder builder = new TransactionCreatorBuilder();

        return builder
                .addOperation(MakeCheese.getInstance())
                .addContext(simulation)
                .setName("Milk_Processor" + milkProcessorCount++)
                .build();
    }

    public TransactionCreator createOilProducer() {
        TransactionCreatorBuilder builder = new TransactionCreatorBuilder();

        return builder
                .addOperation(MakeOil.getInstance())
                .addContext(simulation)
                .setName("Oil_Producer" + oilProducerCount++)
                .build();
    }

    public TransactionCreator createPickler() {
        TransactionCreatorBuilder builder = new TransactionCreatorBuilder();

        return builder
                .addOperation(PickleOlive.getInstance())
                .addContext(simulation)
                .setName("Pickler" + picklerCount++)
                .build();
    }

    public TransactionCreator createDistributor() {
        TransactionCreatorBuilder builder = new TransactionCreatorBuilder();

        return builder
                .addOperation(DistributeMeat.getInstance())
                .addOperation(DistributeMilkProduct.getInstance())
                .addOperation(DistributeOrangeProduct.getInstance())
                .addOperation(DistributeAppleProduct.getInstance())
                .addOperation(DistributeOliveProduct.getInstance())
                .addContext(simulation)
                .setName("Distributor" + distributorCount++)
                .build();
    }

    public TransactionCreator createRestaurant() {
        TransactionCreatorBuilder builder = new TransactionCreatorBuilder();

        return builder
                .addOperation(MakeMeal.getInstance())
                .addContext(simulation)
                .setName("Restaurant" + restaurantsCount++)
                .build();
    }
}
