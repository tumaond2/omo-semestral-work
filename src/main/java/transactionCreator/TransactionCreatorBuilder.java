package transactionCreator;

import operation.Operation;
import simulation.Simulation;

public class TransactionCreatorBuilder {

    final TransactionCreator product = new TransactionCreator();

    public TransactionCreatorBuilder() {

    }

    public TransactionCreator build() {
        return product;
    }

    public TransactionCreatorBuilder addOperation(Operation operation) {
        product.addOperation(operation);

        return this;
    }

    public TransactionCreatorBuilder addContext(Simulation simulation) {
        product.addContext(simulation);

        return this;
    }

    public TransactionCreatorBuilder setName(String name) {
        product.setName(name);
        return this;
    }

}
