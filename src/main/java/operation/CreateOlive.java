package operation;

import commodity.Commodity;
import commodity.Olive;
import transactionCreator.TransactionCreator;

public class CreateOlive extends Create<Olive> {

    final static Integer RAW_OLIVE_VALUE = 20;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        Commodity olive;
        if (currentState == null) {
            olive = new Olive();
        } else {
            olive = currentState;
        }
        olive.setValue(RAW_OLIVE_VALUE);

        return olive;
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue().equals(RAW_OLIVE_VALUE);
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(RAW_OLIVE_VALUE);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(RAW_OLIVE_VALUE);
        return source;
    }

    public static CreateOlive getInstance() {
        return Operation.getInstance(CreateOlive.class);
    }

    @Override
    public Integer getTicks() {
        return 2;
    }

    @Override
    public Integer getPrice() {
        return RAW_OLIVE_VALUE;
    }
}
