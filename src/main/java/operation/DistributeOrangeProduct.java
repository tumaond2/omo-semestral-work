package operation;

import commodity.Commodity;
import commodity.Orange;
import transactionCreator.TransactionCreator;

public class DistributeOrangeProduct extends Create<Orange> {

    final static Integer DISTRIBUTE_ORANGE_PRODUCT = 30;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return increaseValue(currentState);
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue() >= DISTRIBUTE_ORANGE_PRODUCT;
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(DISTRIBUTE_ORANGE_PRODUCT);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(DISTRIBUTE_ORANGE_PRODUCT);
        return source;
    }

    public static DistributeOrangeProduct getInstance() {
        return Operation.getInstance(DistributeOrangeProduct.class);
    }

    @Override
    public Integer getTicks() {
        return 1;
    }

    @Override
    public Integer getPrice() {
        return DISTRIBUTE_ORANGE_PRODUCT;
    }
}