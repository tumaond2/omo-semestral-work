package operation;

import commodity.Apple;
import commodity.Commodity;
import transactionCreator.TransactionCreator;

public class DistributeAppleProduct extends Create<Apple> {

    final static Integer DISTRIBUTE_APPLE_PRODUCT = 30;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return increaseValue(currentState);
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue() >= DISTRIBUTE_APPLE_PRODUCT;
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(DISTRIBUTE_APPLE_PRODUCT);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(DISTRIBUTE_APPLE_PRODUCT);
        return source;
    }

    public static DistributeAppleProduct getInstance() {
        return Operation.getInstance(DistributeAppleProduct.class);
    }

    @Override
    public Integer getTicks() {
        return 1;
    }

    @Override
    public Integer getPrice() {
        return DISTRIBUTE_APPLE_PRODUCT;
    }
}
