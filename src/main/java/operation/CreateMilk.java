package operation;

import commodity.Commodity;
import commodity.Milk;
import transactionCreator.TransactionCreator;

public class CreateMilk extends Create<Milk> {

    final static Integer PURE_MILK_VALUE = 50;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        Commodity milk;
        if (currentState == null) {
            milk = new Milk();
        } else {
            milk = currentState;
        }

        milk.setValue(PURE_MILK_VALUE);
        return milk;
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue().equals(PURE_MILK_VALUE);
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(PURE_MILK_VALUE);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(PURE_MILK_VALUE);
        return source;
    }

    public static CreateMilk getInstance() {
        return Operation.getInstance(CreateMilk.class);
    }

    @Override
    public Integer getTicks() {
        return 1;
    }

    @Override
    public Integer getPrice() {
        return PURE_MILK_VALUE;
    }
}
