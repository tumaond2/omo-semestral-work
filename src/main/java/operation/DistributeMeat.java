package operation;

import commodity.Commodity;
import commodity.Meat;
import transactionCreator.TransactionCreator;

public class DistributeMeat extends Create<Meat> {

    final static Integer DISTRIBUTE_MEAT = 50;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return increaseValue(currentState);
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue() >= DISTRIBUTE_MEAT;
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(DISTRIBUTE_MEAT);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(DISTRIBUTE_MEAT);
        return source;
    }

    public static DistributeMeat getInstance() {
        return Operation.getInstance(DistributeMeat.class);
    }

    @Override
    public Integer getTicks() {
        return 1;
    }

    @Override
    public Integer getPrice() {
        return DISTRIBUTE_MEAT;
    }
}
