package operation;

import commodity.Commodity;
import commodity.Orange;
import transactionCreator.TransactionCreator;

public class CreateOrange extends Create<Orange> {

    final static Integer RAW_ORANGE_VALUE = 10;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        Commodity orange;
        if (currentState == null) {
            orange = new Orange();
        } else {
            orange = currentState;
        }
        orange.setValue(RAW_ORANGE_VALUE);

        return orange;
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue().equals(RAW_ORANGE_VALUE);
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(RAW_ORANGE_VALUE);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(RAW_ORANGE_VALUE);
        return source;
    }

    public static CreateOrange getInstance() {
        return Operation.getInstance(CreateOrange.class);
    }

    @Override
    public Integer getTicks() {
        return 2;
    }

    @Override
    public Integer getPrice() {
        return RAW_ORANGE_VALUE;
    }
}