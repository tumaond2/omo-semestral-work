package operation;

import commodity.Commodity;
import commodity.Milk;
import transactionCreator.TransactionCreator;

public class DistributeMilkProduct extends Create<Milk> {

    final static Integer DISTRIBUTE_MILK_PRODUCT = 50;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return increaseValue(currentState);
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue() >= DISTRIBUTE_MILK_PRODUCT;
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(DISTRIBUTE_MILK_PRODUCT);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(DISTRIBUTE_MILK_PRODUCT);
        return source;
    }

    public static DistributeMilkProduct getInstance() {
        return Operation.getInstance(DistributeMilkProduct.class);
    }

    @Override
    public Integer getTicks() {
        return 1;
    }

    @Override
    public Integer getPrice() {
        return DISTRIBUTE_MILK_PRODUCT;
    }
}
