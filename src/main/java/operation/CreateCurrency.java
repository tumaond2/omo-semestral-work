package operation;

import commodity.Commodity;
import commodity.Currency;
import transactionCreator.TransactionCreator;

public class CreateCurrency extends Create<Currency> {

    Integer ticks = 1;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return new Currency();
    }

    public static CreateCurrency getInstance() {
        return Operation.getInstance(CreateCurrency.class);
    }
}
