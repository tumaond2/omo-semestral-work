package operation;

import commodity.Apple;
import commodity.Commodity;
import transactionCreator.TransactionCreator;

public class MakeAppleJuice extends Create<Apple> {

    final static Integer PROCESS_PRICE = 30;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return increaseValue(currentState);
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue() >= PROCESS_PRICE;
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(PROCESS_PRICE);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(PROCESS_PRICE);
        return source;
    }

    public static MakeAppleJuice getInstance() {
        return Operation.getInstance(MakeAppleJuice.class);
    }

    @Override
    public Integer getTicks() {
        return 2;
    }

    @Override
    public Integer getPrice() {
        return PROCESS_PRICE;
    }
}
