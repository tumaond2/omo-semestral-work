package operation;

import commodity.Commodity;
import commodity.Meat;
import transactionCreator.TransactionCreator;

public class MakeMeal extends Create<Meat> {

    final static Integer COOK_MEAT = 70;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return increaseValue(currentState);
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue() >= COOK_MEAT;
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(COOK_MEAT);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(COOK_MEAT);
        return source;
    }

    public static MakeMeal getInstance() {
        return Operation.getInstance(MakeMeal.class);
    }

    @Override
    public Integer getTicks() {
        return 1;
    }

    @Override
    public Integer getPrice() {
        return COOK_MEAT;
    }
}
