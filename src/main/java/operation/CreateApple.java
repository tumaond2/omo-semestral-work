package operation;

import commodity.Apple;
import commodity.Commodity;
import transactionCreator.TransactionCreator;

public class CreateApple extends Create<Apple> {

    final static Integer RAW_APPLE_VALUE = 10;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        Commodity apple;
        if (currentState == null) {
            apple = new Apple();
        } else {
            apple = currentState;
        }
        apple.setValue(RAW_APPLE_VALUE);

        return apple;
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue().equals(RAW_APPLE_VALUE);
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(RAW_APPLE_VALUE);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(RAW_APPLE_VALUE);
        return source;
    }

    public static CreateApple getInstance() {
        return Operation.getInstance(CreateApple.class);
    }

    @Override
    public Integer getTicks() {
        return 2;
    }

    @Override
    public Integer getPrice() {
        return RAW_APPLE_VALUE;
    }
}
