package operation;

import commodity.Commodity;
import commodity.Olive;
import transactionCreator.TransactionCreator;

public class DistributeOliveProduct extends Create<Olive> {

    final static Integer DISTRIBUTE_OLIVE_PRODUCT = 20;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return increaseValue(currentState);
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue() >= DISTRIBUTE_OLIVE_PRODUCT;
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(DISTRIBUTE_OLIVE_PRODUCT);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(DISTRIBUTE_OLIVE_PRODUCT);
        return source;
    }

    public static DistributeOliveProduct getInstance() {
        return Operation.getInstance(DistributeOliveProduct.class);
    }

    @Override
    public Integer getTicks() {
        return 1;
    }

    @Override
    public Integer getPrice() {
        return DISTRIBUTE_OLIVE_PRODUCT;
    }
}
