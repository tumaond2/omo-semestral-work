package operation;

import commodity.Commodity;
import commodity.Meat;
import transactionCreator.TransactionCreator;

public class CreateMeat extends Create<Meat> {

    final static Integer RAW_MEAT_VALUE = 50;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        Commodity meat;
        if (currentState == null) {
            meat = new Meat();
        } else {
            meat = currentState;
        }
//        Meat meat =  new Meat();
        meat.setValue(RAW_MEAT_VALUE);

        return meat;
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue().equals(RAW_MEAT_VALUE);
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(RAW_MEAT_VALUE);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(RAW_MEAT_VALUE);
        return source;
    }

    public static CreateMeat getInstance() {
        return Operation.getInstance(CreateMeat.class);
    }

    @Override
    public Integer getTicks() {
        return 3;
    }

    @Override
    public Integer getPrice() {
        return RAW_MEAT_VALUE;
    }
}
