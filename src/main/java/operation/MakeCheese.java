package operation;

import commodity.Commodity;
import commodity.Milk;
import transactionCreator.TransactionCreator;

public class MakeCheese extends Create<Milk> {

    final static Integer CREATE_CHEESE = 50;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return increaseValue(currentState);
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue() >= CREATE_CHEESE;
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(CREATE_CHEESE);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(CREATE_CHEESE);
        return source;
    }

    public static MakeCheese getInstance() {
        return Operation.getInstance(MakeCheese.class);
    }

    @Override
    public Integer getTicks() {
        return 4;
    }

    @Override
    public Integer getPrice() {
        return CREATE_CHEESE;
    }
}
