package operation;

import commodity.Commodity;
import commodity.Olive;
import transactionCreator.TransactionCreator;

public class PickleOlive extends Create<Olive> {

    final static Integer PROCESS_PRICE = 50;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return increaseValue(currentState);
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue() >= PROCESS_PRICE;
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(PROCESS_PRICE);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(PROCESS_PRICE);
        return source;
    }

    public static PickleOlive getInstance() {
        return Operation.getInstance(PickleOlive.class);
    }

    @Override
    public Integer getTicks() {
        return 4;
    }

    @Override
    public Integer getPrice() {
        return PROCESS_PRICE;
    }
}
