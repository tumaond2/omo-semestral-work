package operation;

import commodity.Commodity;
import transactionCreator.TransactionCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The implementation should be singleton, I guess? Implemented generic singleton.
 * If there would be multiple people with the same operation, you should be able to search them by the operation, so there should be only one instance of single operation type. Hence singleton.
 */
@SuppressWarnings("CanBeFinal")
public class Operation {

    public Integer ticks = 1;

    List<TransactionCreator> currentlyAvailable = new ArrayList<>();

    protected Operation() {

    }

    private static final Operation instance = new Operation();

    private static final Logger LOG = Logger.getLogger(Operation.class.getName());

    @SuppressWarnings("rawtypes")
    private final Map<Class, Object> mapHolder = new HashMap<>();

    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    /**
     * Usage as 'Operation createCurrency = Operation.getInstance(CreateCurrency.class)';
     *
     * @param classOf class of which to create singleton
     * @param <T>     Return Type of generic singleton
     * @return instance of singleton
     */
    @SuppressWarnings("unchecked")
    protected static <T> T getInstance(Class<T> classOf) {
        synchronized (instance) {

            if (!instance.mapHolder.containsKey(classOf)) {
                try {
                    @SuppressWarnings("deprecation") T obj = classOf.newInstance();
                    instance.mapHolder.put(classOf, obj);
                } catch (Exception e) {
                    LOG.log(Level.ALL, "Operation instantiation error");
                    System.exit(1);
                }
            }
            return (T) instance.mapHolder.get(classOf);
        }
    }

    @SuppressWarnings("SameReturnValue")
    public static Operation getInstance() {
        return null;
    }

    /**
     * Applies transaction if it is valid, returns previous state otherwise
     *
     * @param currentState current state of commodity
     * @param origin       as whom to try applying the Operation to given commodity
     * @return new Commodity state
     */
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return null;
    }

    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return true;
    }

    public Commodity decreaseValue(Commodity source) {
        return source;
    }

    public Commodity increaseValue(Commodity source) {
        return source;
    }

    public void addCreator(TransactionCreator transactionCreator) {
        currentlyAvailable.add(transactionCreator);
    }

    public List<TransactionCreator> getTransactionCreators() {
        return currentlyAvailable;
    }

    public Integer getTicks() {
        return ticks;
    }

    public Integer getPrice() {
        return 0;
    }
}
