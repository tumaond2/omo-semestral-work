package operation;

import commodity.Commodity;
import transactionCreator.TransactionCreator;

public class Initialize extends Operation {
    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return currentState;
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue().equals(0);
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        return source;
    }

    public static Initialize getInstance() {
        return Operation.getInstance(Initialize.class);
    }

    @Override
    public Integer getTicks() {
        return 0;
    }

    @Override
    public Integer getPrice() {
        return 0;
    }
}
