package operation;

import commodity.Commodity;
import commodity.Orange;
import transactionCreator.TransactionCreator;

public class MakeOrangeJuice extends Create<Orange> {

    final static Integer PROCESS_PRICE = 30;

    @Override
    public Commodity apply(Commodity currentState, TransactionCreator origin) {
        return increaseValue(currentState);
    }

    @Override
    public boolean canBeApplied(Commodity currentState, Commodity money, TransactionCreator origin) {
        return money.getValue() >= PROCESS_PRICE;
    }

    @Override
    public Commodity decreaseValue(Commodity source) {
        source.decreaseValue(PROCESS_PRICE);
        return source;
    }

    @Override
    public Commodity increaseValue(Commodity source) {
        source.increaseValue(PROCESS_PRICE);
        return source;
    }

    public static MakeOrangeJuice getInstance() {
        return Operation.getInstance(MakeOrangeJuice.class);
    }

    @Override
    public Integer getTicks() {
        return 2;
    }

    @Override
    public Integer getPrice() {
        return PROCESS_PRICE;
    }
}