package commodity;

public abstract class Commodity {

    Integer value = 0;

    public abstract Commodity clone();

    public void setValue(Integer value) {
        this.value = value;
    }

    public void increaseValue(Integer value) {
        this.value += value;
    }

    public void decreaseValue(Integer value) {
        this.value -= value;
    }

    public String toString() {
        return getClass().getName();
    }

    public Integer getValue() {
        return value;
    }


}
