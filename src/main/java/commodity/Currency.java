package commodity;

public class Currency extends Commodity {
    @Override
    public Commodity clone() {
        return null;
    }

    @Override
    public String toString() {
        return "Currency " + value;
    }
}
