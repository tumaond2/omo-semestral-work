# FOOD CHAIN

## Project Info: 
### Premise
#### Assignment
we have chosen [FOOD CHAIN](https://cw.fel.cvut.cz/b201/_media/courses/b6b36omo/hw/projekt_food_chain.docx) project as our semestral work.
#### Target
we ought to build system capable of producing Commodities on-demand basis, **mimicking behavior of Ledger technology** with features as stated below:
- prevent double spending
- produce logs for each Commodity instance
- produce logs for each tick
- produce logs for double spending

### Team members:
| Name  	| Profile  	        |
|---	        |---	        |
| Ondřej Tůma  	| @tumaond2  	|
| Jan Feber  	| @feberja1  	|
### Diagrams:
- [UML](https://lucid.app/documents/view/26a19722-9edb-406d-9ffc-39cd62ea4aa0)

### Quick start

Run app: `mvn install && java -cp target/omo-semestral-work-1.0-SNAPSHOT.jar Entrypoint`

Generate javadoc:
`mvn javadoc:javadoc`


#### Functional requirements
- [x] F1 
    - src/main/java/transactionCreator/*
- [x] F2
    - předávání: src/main/java/simulation/Request
    - operace: src/main/java/operation/*
    - transakce: src/main/java/transaction/Transaction
- [x] F3
    - ledger: src/main/java/transaction/TransactionPool
- [x] F4
    - viz. f3, vše je v ledgeru.
    - generování: src/main/java/report/ReportHandler:get{reportName}Report()
- [x] F5
    - kanál: src/main/java/channel/*
    - operace: produkt je posloupností událostí na dané komoditě, má nadefinované aktivní kanály. src/main/java/product/Product
- [x] F6
    - detekce na atomické úrovni: src/main/java/transaction/TransactionPool:isDoubleSpending()
    - detekce při běhu simulace: src/main/java/simulation/Request:generateTransaction()
    - detekce při generování reportu: src/main/java/report/ReportHandler:getDoubleSpendingReport() 
- [x] F7
    - transactionPool se chová jako stream transackí. Má pouze metodu na vkládání dalších transakcí na konec. Nic jako vkládání dorostřed udělat z principu věci nejde.
- [x] F8
    - obdoba Promise v Javascriptu: src/main/java/simulation/Request
- [x] F9
    - Po konzultaci na Váš návrh řešeno přímým předáváním. viz src/main/java/transaction/TransactionCreator:requestCommodity()
- [x] F10
    - src/main/java/report/ReportHandler:getTransactionReport()
    - src/main/java/report/ReportHandler:getFoodChainReport()
    - src/main/java/report/ReportHandler:getTransactionReport()
    - src/main/java/report/ReportHandler:getDoubleSpendingReport()

#### Non-functional requirements
- [x] autentizace - nepožadována ✓
- [x] nemá gui ✓
- [x] centralni autorita neexistuje ✓
- [x] metody jsou skryty 🙈
- [x] src/main/java/transaction/TransactionCreator/Certificate
- [x] po spuštění záplava souborů ✓
- [x] konfigurace je kódem ✓

#### Design patterns
- Observer - src/main/java/simulation/Simulation
- Factory -  src/main/java/transaction/transactionCreator/TransactionCreatorFactory
- Builder -  src/main/java/transaction/transactionCreator/TransactionCreatorBuilder
- Singleton -  src/main/java/transaction/Operation
- Chain of Responsibility - - Factory -  src/main/java/simulation/Request
- Memento - src/main/java/operation/Operation na src/main/java/commodity/Commodity
- Map, Filter, Reduce - src/main/java/transaction/TransactionPool
- Template method - src/main/java/transaction/TransactionCreator::nextTickPhase1,2,3


#### Required outcomes
    - [x] Simulation cases in src/test/java/simulation/SimulationTest